function counterFactory() { 
    let count = 10
    // increment function
    function increment(count) {
        return count++
    }
   // decrement function
    function decrement(count) {
        return count--
    }
  return { increment,decrement}
}
// export the code
module.exports = counterFactory